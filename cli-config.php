<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use App\AppKernel;

(new Dotenv())->load(__DIR__ . '/.env');
$kernel = new AppKernel();
$kernel->boot();
$container = $kernel->container;

return ConsoleRunner::createHelperSet($container->get('doctrine.orm.entity_manager'));
