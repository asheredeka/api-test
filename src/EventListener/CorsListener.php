<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CorsListener
{
    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * CorsListener constructor.
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if (HttpKernelInterface::MASTER_REQUEST !== $event->getRequestType()) {
            return;
        }

        $request = $event->getRequest();

        $this->dispatcher->addListener('kernel.response', array($this, 'forceAccessControlAllowOriginHeader'), -1);

        if (!$request->headers->has('Origin') || $request->headers->get('Origin') == $request->getSchemeAndHttpHost()) {
            return;
        }

        if ('OPTIONS' === $request->getMethod() && $request->headers->has('Access-Control-Request-Method')) {
            $event->setResponse($this->getPreflightResponse($request));
            return;
        }
    }

    /**
     * @param ResponseEvent $event
     */
    public function forceAccessControlAllowOriginHeader(ResponseEvent $event)
    {
        $event->getResponse()->headers->set('Access-Control-Allow-Origin', $event->getRequest()->headers->get('Origin'));
        $event->getResponse()->headers->set('Access-Control-Allow-Headers', 'X-File-Upload, content-type');
        $event->getResponse()->headers->set('Access-Control-Allow-Methods', '*');
        $event->getResponse()->headers->set('Access-Control-Allow-Credentials', 'true');
    }

    /**
     * @param Request $request
     * @return Response
     */
    protected function getPreflightResponse(Request $request)
    {
        $response = new Response();
        $response->setVary(['Origin']);

        $response->headers->set('Access-Control-Allow-Origin', $request->headers->get('Origin'));
        $response->headers->set('Access-Control-Allow-Headers', 'X-File-Upload, content-type');
        $response->headers->set('Access-Control-Allow-Methods', '*');
        $response->headers->set('Access-Control-Allow-Credentials', 'true');

        return $response;
    }
}
