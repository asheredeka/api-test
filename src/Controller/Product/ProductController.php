<?php

declare(strict_types=1);

namespace App\Controller\Product;

use App\Controller\Controller;
use App\Entity\Product;
use App\Service\Product\ProductGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ProductController extends Controller
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return JsonResponse
     */
    public function index(EntityManagerInterface $entityManager, Request $request): JsonResponse
    {
        $perPage = $request->get('perPage', 10);
        $page = $request->get('page', 1);

        $products = $entityManager->getRepository(Product::class)->findBy(
            [],
            null,
            $perPage,
            $page * $perPage - $perPage
        );

        return $this->json([
            'data' => $products
        ], Response::HTTP_OK, [], ['groups' => 'products_list']);
    }

    /**
     * @param ProductGeneratorInterface $productGenerator
     * @param Request $request
     * @return JsonResponse
     */
    public function generate(ProductGeneratorInterface $productGenerator, Request $request): JsonResponse
    {
        $count = $request->get('count', 20);

        try {
            $productGenerator->generate($count);
        } catch (\Throwable $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }

        return $this->json([]);
    }
}
