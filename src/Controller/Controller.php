<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

abstract class Controller implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param array $data
     * @param int $status
     * @param array $headers
     * @param array $context
     * @return JsonResponse
     */
    protected function json(array $data, int $status = 200, array $headers = [], array $context = []): JsonResponse
    {
        if ($this->container->has(Serializer::class)) {
            $json = $this->container->get(Serializer::class)->serialize($data, 'json', array_merge([
                'json_encode_options' => JsonResponse::DEFAULT_ENCODING_OPTIONS
            ], $context));

            return new JsonResponse($json, $status, $headers, true);
        }

        return new JsonResponse($data, $status, $headers);
    }
}
