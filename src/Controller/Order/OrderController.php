<?php

declare(strict_types=1);

namespace App\Controller\Order;

use App\Controller\Controller;
use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrderController extends Controller
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return JsonResponse
     */
    public function create(EntityManagerInterface $entityManager, Request $request): JsonResponse
    {
        $productIds = $request->get('product_ids', []);

        if (count($productIds) === 0) {
            throw new BadRequestHttpException('Need to specify product ids');
        }

        $products = $entityManager->getRepository(Product::class)->findBy(['id' => $productIds]);

        if (count($products) !== count(array_unique($productIds))) {
            throw new BadRequestHttpException('Some products do not exists');
        }

        try {
            $order = $entityManager->getRepository(Order::class)->create($products);

            return $this->json([
                'data' => $order->getId()
            ]);
        } catch (\Throwable $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }
}
