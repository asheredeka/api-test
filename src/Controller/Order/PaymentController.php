<?php

declare(strict_types=1);

namespace App\Controller\Order;

use App\Controller\Controller;
use App\Entity\Order;
use App\Service\Order\Payment\PaymentException;
use App\Service\Order\Payment\PaymentManagerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PaymentController extends Controller
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param PaymentManagerInterface $paymentManager
     * @param Request $request
     * @param int $orderId
     * @return JsonResponse
     */
    public function pay(
        EntityManagerInterface $entityManager,
        PaymentManagerInterface $paymentManager,
        Request $request,
        int $orderId
    ): JsonResponse {
        $order = $entityManager->getRepository(Order::class)->find($orderId);
        $amount = $request->get('amount', 0);

        if ($order === null) {
            throw new BadRequestHttpException(sprintf('Order #%s not found.', $orderId));
        }

        if ($order->getStatus() !== Order::ORDER_STATUS_NEW) {
            throw new BadRequestHttpException('You can`t pay this order.');
        }

        if ((string)$order->getAmount() !== (string)$amount) {
            throw new BadRequestHttpException('Amount must be equals to order amount');
        }

        try {
            $paymentManager->pay($order);
        } catch (PaymentException $paymentException) {
            throw new BadRequestHttpException($paymentException->getMessage());
        } catch (\Throwable $exception) {
            throw new BadRequestHttpException('Unknown error. Try later.');
        }

        return $this->json([]);
    }
}
