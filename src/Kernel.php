<?php

declare(strict_types=1);

namespace App;

use App\DependencyInjection\Extensions\Doctrine\DoctrineExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Compiler\ResolveEnvPlaceholdersPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DependencyInjection\ControllerArgumentValueResolverPass;
use Symfony\Component\HttpKernel\DependencyInjection\RegisterControllerArgumentLocatorsPass;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Serializer\DependencyInjection\SerializerPass;

abstract class Kernel
{
    /**
     * @var ContainerInterface
     */
    public $container;

    public function boot(): void
    {
        $this->container = $this->buildContainer();
    }

    /**
     * @param Request $request
     * @param int $type
     * @param bool $catch
     * @return Response
     */
    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true): Response
    {
        $this->boot();

        /** @var HttpKernelInterface $httpKernel */
        $httpKernel = $this->container->get(HttpKernel::class);

        return $httpKernel->handle($request, $type, $catch);
    }

    /**
     * @return ContainerInterface
     */
    private function buildContainer(): ContainerInterface
    {
        $container = new ContainerBuilder();

        $this->addExtensions($container);
        $this->addKernelParameters($container);
        $this->addCompilerPasses($container);

        $loader = new YamlFileLoader($container, new FileLocator([__DIR__ . '/../config']));
        $loader->load('services.yaml');

        $container->compile();

        return $container;
    }

    /**
     * @param ContainerInterface $container
     */
    private function addKernelParameters(ContainerInterface $container): void
    {
        $container->setParameter('kernel.project_dir', realpath(__DIR__ . '/..'));
        $container->setParameter('kernel.debug', false);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addCompilerPasses(ContainerInterface $container): void
    {
        $container->addCompilerPass(new ResolveEnvPlaceholdersPass());
        $container->addCompilerPass(new RegisterControllerArgumentLocatorsPass());
        $container->addCompilerPass(new ControllerArgumentValueResolverPass());
        $container->addCompilerPass(new SerializerPass());
    }

    /**
     * @param ContainerInterface $container
     */
    private function addExtensions(ContainerInterface $container): void
    {
        $container->registerExtension(new DoctrineExtension());
    }
}
