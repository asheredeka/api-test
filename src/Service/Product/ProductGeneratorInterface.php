<?php

declare(strict_types=1);

namespace App\Service\Product;

interface ProductGeneratorInterface
{
    /**
     * @param int $count
     */
    public function generate(int $count): void;
}
