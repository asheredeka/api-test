<?php

declare(strict_types=1);

namespace App\Service\Product;

use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;

class ProductGenerator implements ProductGeneratorInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Generator $faker
     */
    private $faker;

    /**
     * ProductGenerator constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->faker = Factory::create();
    }

    /**
     * @param int $count
     */
    public function generate(int $count): void
    {
        $products = [];

        for ($i = 0; $i < $count; $i++) {
            $product = new Product();
            $product->setName($this->faker->word);
            $product->setPrice($this->faker->randomFloat(2, 300, 30000));

            $this->entityManager->persist($product);
            $products[] = $product;
        }

        $this->entityManager->flush($products);
    }
}
