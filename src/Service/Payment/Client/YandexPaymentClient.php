<?php

declare(strict_types=1);

namespace App\Service\Payment\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\Response;

class YandexPaymentClient implements PaymentClientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * YandexPaymentClient constructor.
     * @param string $endpoint
     */
    public function __construct(string $endpoint)
    {
        $this->client = new Client([
            'base_uri' => $endpoint
        ]);
    }

    /**
     * @inheritDoc
     */
    public function pay(array $parameters = []): Response
    {
        try {
            $this->client->get('/');

            return new Response('Paid.', Response::HTTP_OK);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();

            return new Response($response->getBody()->getContents(), $response->getStatusCode(), $response->getHeaders());
        } catch (\Throwable $exception) {
            return new Response('Payment failed. Try later.', Response::HTTP_BAD_REQUEST);
        }
    }
}
