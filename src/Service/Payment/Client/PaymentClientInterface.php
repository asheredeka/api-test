<?php

declare(strict_types=1);

namespace App\Service\Payment\Client;

use Symfony\Component\HttpFoundation\Response;

interface PaymentClientInterface
{
    /**
     * @param array $parameters
     * @return Response
     */
    public function pay(array $parameters = []): Response;
}
