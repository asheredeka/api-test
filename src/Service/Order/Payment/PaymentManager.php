<?php

declare(strict_types=1);

namespace App\Service\Order\Payment;

use App\Entity\PaidInterface;
use App\Service\Payment\Client\PaymentClientInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class PaymentManager implements PaymentManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PaymentClientInterface
     */
    private $paymentClient;

    /**
     * PaymentManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param PaymentClientInterface $paymentClient
     */
    public function __construct(EntityManagerInterface $entityManager, PaymentClientInterface $paymentClient)
    {
        $this->entityManager = $entityManager;
        $this->paymentClient = $paymentClient;
    }

    /**
     * @inheritDoc
     * @throws PaymentException
     */
    public function pay(PaidInterface $paid): void
    {
        $response = $this->paymentClient->pay();

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new PaymentException();
        }

        $paid->markPaid();

        $this->entityManager->persist($paid);
        $this->entityManager->flush($paid);
    }
}
