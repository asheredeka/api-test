<?php

declare(strict_types=1);

namespace App\Service\Order\Payment;

use App\Entity\PaidInterface;

interface PaymentManagerInterface
{
    /**
     * @param PaidInterface $paid
     */
    public function pay(PaidInterface $paid): void;
}
