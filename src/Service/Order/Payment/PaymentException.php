<?php

declare(strict_types=1);

namespace App\Service\Order\Payment;

use Exception;
use Throwable;

class PaymentException extends Exception
{
    /**
     * PaymentException constructor.
     * @inheritDoc
     */
    public function __construct($message = "Payment failed. Try later.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
