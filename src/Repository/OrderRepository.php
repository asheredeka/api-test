<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\Product;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository
{
    /**
     * @param Product[] $products
     * @return Order
     */
    public function create(array $products): Order
    {
        $order = new Order();

        $this->setOrderProducts($order, $products);
        $this->getEntityManager()->persist($order);
        $this->getEntityManager()->flush($order);

        return $order;
    }

    /**
     * @param Order $order
     * @param Product[] $products
     */
    private function setOrderProducts(Order $order, array $products): void
    {
        $orderProducts = [];

        foreach ($products as $product) {
            $orderProduct = new OrderProduct();
            $orderProduct->setOrder($order);
            $orderProduct->setProduct($product);
            $orderProduct->setPrice($product->getPrice());

            $orderProducts[] = $orderProduct;
        }

        $order->setOrderProducts($orderProducts);
    }
}
