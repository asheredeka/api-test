<?php

declare(strict_types=1);

namespace App\Entity;

interface PaidInterface
{
    public function markPaid(): void;
}
