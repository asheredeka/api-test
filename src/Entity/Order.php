<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use DateTime;

/**
 * Class Order
 *
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 * @ORM\HasLifecycleCallbacks()
 */
class Order implements PaidInterface
{
    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_PAID = 'paid';

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", name="id")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="status", length=10)
     */
    private $status = self::ORDER_STATUS_NEW;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\OrderProduct", mappedBy="order", cascade={"persist"})
     */
    private $orderProducts;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    /**
     * Order constructor.
     */
    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return OrderProduct[]
     */
    public function getOrderProducts()
    {
        return $this->orderProducts->toArray();
    }

    /**
     * @param OrderProduct[]|ArrayCollection|PersistentCollection $orderProducts
     */
    public function setOrderProducts($orderProducts): void
    {
        $this->orderProducts->clear();

        foreach ($orderProducts as $orderProduct) {
            $this->orderProducts->add($orderProduct);
        }
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onPrePersist(): void
    {
        $this->setCreatedAt(new DateTime());
    }

    public function markPaid(): void
    {
        $this->setStatus(self::ORDER_STATUS_PAID);
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        $amount = 0;

        foreach ($this->getOrderProducts() as $orderProduct) {
            $amount += $orderProduct->getPrice();
        }

        return $amount;
    }
}
