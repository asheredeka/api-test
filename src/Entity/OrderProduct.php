<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class OrderProduct
 *
 * @ORM\Entity()
 * @ORM\Table(name="order_products")
 */
class OrderProduct
{
    /**
     * @var Product
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", nullable=false, referencedColumnName="id")
     */
    private $product;

    /**
     * @var Order
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="App\Entity\Order", inversedBy="orderProducts")
     * @ORM\JoinColumn(name="order_id", onDelete="CASCADE", nullable=false, referencedColumnName="id")
     */
    private $order;

    /**
     * @var float
     *
     * @ORM\Column(type="float", name="price")
     */
    private $price = 0;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order): void
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }
}
