<?php

declare(strict_types=1);

namespace App\DependencyInjection\Extensions\Doctrine;

use Doctrine\Common\Proxy\AbstractProxyFactory;
use Doctrine\ORM\Proxy\Autoloader;
use Doctrine\ORM\Tools\Setup;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class DoctrineExtension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        foreach ($config['migrations'] as $name => $value) {
            $container->setParameter(sprintf('doctrine.migrations.%s', $name), $value);
        }

        foreach ($config['dbal'] as $name => $value) {
            $parameterValue = $container->resolveEnvPlaceholders($value, true);
            $container->setParameter(sprintf('doctrine.dbal.%s', $name), $parameterValue);
        }

        $ormDir = $config['orm']['dir'];
        $proxyDir = $config['orm']['proxy_dir'];

        $ormConfiguration = Setup::createAnnotationMetadataConfiguration([$ormDir], false, $proxyDir, null, false);
        $ormConfiguration->setAutoGenerateProxyClasses(AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS);

        $connection = [
            'driver' => $container->getParameter('doctrine.dbal.driver'),
            'user' => $container->getParameter('doctrine.dbal.user'),
            'password' => $container->getParameter('doctrine.dbal.password'),
            'dbname' => $container->getParameter('doctrine.dbal.dbname'),
            'host' => $container->getParameter('doctrine.dbal.host'),
            'port' => $container->getParameter('doctrine.dbal.port'),
            'charset' => $container->getParameter('doctrine.dbal.charset')
        ];

        $container
            ->setDefinition('doctrine.orm.entity_manager', new ChildDefinition('doctrine.orm.entity_manager.abstract'))
            ->setArguments([
                $connection,
                $ormConfiguration
            ])
            ->setPublic(true);
    }
}
