<?php

declare(strict_types=1);

namespace App\DependencyInjection\Extensions\Doctrine;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder('doctrine');
        $root = $builder->getRootNode();

        $root
            ->children()
                ->arrayNode('dbal')
                    ->children()
                        ->scalarNode('driver')->isRequired()->end()
                        ->scalarNode('dbname')->isRequired()->end()
                        ->scalarNode('user')->isRequired()->end()
                        ->scalarNode('password')->isRequired()->end()
                        ->scalarNode('host')->isRequired()->end()
                        ->scalarNode('port')->isRequired()->end()
                        ->scalarNode('charset')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('orm')
                    ->children()
                        ->scalarNode('dir')->isRequired()->end()
                        ->scalarNode('proxy_dir')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('migrations')
                    ->children()
                        ->scalarNode('name')->isRequired()->end()
                        ->scalarNode('namespace')->isRequired()->end()
                        ->scalarNode('table_name')->isRequired()->end()
                        ->scalarNode('dir')->isRequired()->end()
                    ->end()
                ->end()
            ->end();

        return $builder;
    }
}
