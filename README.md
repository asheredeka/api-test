# API Test

## Установка

1. Скачать репозиторий

    ```git clone git@bitbucket.org:asheredeka/api-test.git```

3. Скопировать файл .env.example в .env, и обновить параметры

## Запуск

1. Запустить докер

    ```docker-compose up -d```

2. Добавить в hosts apitest.loc

3. По умолчанию проект будет доступен по ссылке http://apitest.loc

## Установка пакетов и миграции

1. Переход в контейнер

    ```docker exec -it api-test_php_1 /bin/bash```

2. Запустить composer install

    ```php composer.phar install```

3. Запуск миграций

    ```bin/doctrine-migrations migrations:migrate```
